using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost("NovaVenda")]
        public IActionResult Registrar(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new {Erro = "A data da venda não pode ser vazia!"});
            
            if(venda.Produtos == null)
                return BadRequest(new {CodigoErro = 354, Erro = "A venda deve possuir um produto!" });
            
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        //Aqui implementei o Find id do Vendedor para que não apareça como "null", para isso foi Criado um DbSet para ele na aba Context.
        
        [HttpGet("BuscarVenda{id}")]
        public IActionResult Buscar(int id)
        {
            var venda = _context.Vendas.Find(id);

            var vendedor = _context.Vendedor.Find(id);

            if(venda == null)
                return NotFound();

            return Ok(venda);
        }

    //Optei por exibir uma mensagem de erro caso a atualização de status seja inválida, mostrando os caminhos possíveis.
        [HttpPatch("AtualizarStatus{id}")]
        public IActionResult AtualizarStatus(int id, EnumStatusVenda novoStatus)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();

            if(venda.Status == 0 && novoStatus == (EnumStatusVenda)1  || novoStatus == (EnumStatusVenda)4)
            {
                venda.Status = novoStatus;
            } 
            else if(venda.Status == (EnumStatusVenda)1 && novoStatus == (EnumStatusVenda)2  || novoStatus == (EnumStatusVenda)4){
                venda.Status = novoStatus;
            } 
            else if(venda.Status == (EnumStatusVenda)2 && novoStatus == (EnumStatusVenda)3){
                venda.Status = novoStatus;
            } 
            else{
                return BadRequest(new { Erro = "Você está alterando o Status de forma inválida, os status podem mudar da seguinte forma: " 
            + "Aguardando Pagamento Para: Pagamento Aprovado ou para: Cancelada. " 
            + "Pagamento Aprovado Para: Enviado para Transportadora ou Cancelada. " 
            + "Enviado para Transportadora Para: Entregue"});
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda.Status);
        }
    }
}