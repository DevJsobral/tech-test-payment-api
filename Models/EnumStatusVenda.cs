using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}